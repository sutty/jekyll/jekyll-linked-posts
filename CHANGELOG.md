# Changelog

## v0.2.0 - 2020-09-03

* Breadcrumbs!

## v0.1.0 - 2020-05-20

* First release!
