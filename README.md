# jekyll-linked-posts

This plugin finds the actual posts on front matter fields by UUID, for
comfortable access on Liquid templates.

From v0.4.0 you don't need UUIDs and can use any field name you want
and this plugin will link string or integer values.

We use them for [translations](https://rubygems.org/gems/jekyll-locales)
and manually provided related posts.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'jekyll-linked-posts'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install jekyll-linked-posts

## Usage

Add the plugin to your `_config.yml`:

```yaml
plugins:
- jekyll-linked-posts
```

Every post should have an `uuid` field in its front matter ([Jekyll
calls them "variables"](https://jekyllrb.com/docs/front-matter/), they
can also be "keys").  This field can contain any value, but it **must be
unique to the post**.  [We](https://sutty.nl/en/) use random UUIDs.

```yaml
---
layout: post
title: An article
uuid: 0f892c91-febd-4d76-b9fb-df1e4a47dd38
---
```

To link it from another post, use this value in a field.  It can also be
an array or a hash of UUIDs.

```yaml
---
layout: post
title: Another article
uuid: 8fee66a3-0564-4635-96a6-6881a3e3dbb5
linked: 0f892c91-febd-4d76-b9fb-df1e4a47dd38
linked_array:
- 0f892c91-febd-4d76-b9fb-df1e4a47dd38
linked_hash:
  key: 0f892c91-febd-4d76-b9fb-df1e4a47dd38
---
```

Then, you can call the field in your template as it were a regular
document.

```liquid
<h1>{{ page.title }}</h1>
<p class="linked">{{ page.linked.title }}</p>

<ul>
  {% for linked in page.linked_array %}
    <li>{{ linked.title }}</li>
  {% endfor %}
</ul>
```

To let the plugin know in which fields you want to look for linked
posts, add them as an array to your `_config.yml`:

```yaml
linked_fields:
- linked
- linked_array
- linked_hash
```

**By default, this array contains the values 'lang' and 'related'**.
These values are replaced by your configuration, so if you want to use
them with other fields, re-add them to the configuration.

It also works between collections!

## Breadcrumbs

If you're linking articles by the same field, you can create
a breadcrumb like this:

```html
{%- assign breadcrumbs = page | breadcrumbs: 'previous' -%}

<nav aria-label="Breadcrumbs">
  <ol class="breadcrumb">
    {% for crumb in breadcrumbs %}
      {% if forloop.last %}
        <li class="breadcrumb-item active" aria-current="page">{{ crumb.title }}</li>
      {% else %}
        <li class="breadcrumb-item">
          <a href="{{ crumb.url }}">{{ crumb.title }}</a>
        </li>
      {% endif %}
    {% endfor %}
  </ol>
</nav>
```

## Link by any field name

If you don't want to use UUIDs, you can configure this plugin to link by
any field name as long as their values are strings or integers.

Add to your `_config.yml`:

```yaml
indexed_field: title
```

## Contributing

Bug reports and pull requests are welcome on 0xacab.org at
<https://0xacab.org/sutty/jekyll/jekyll-linked-posts>. This project is
intended to be a safe, welcoming space for collaboration, and
contributors are expected to adhere to the [Sutty code of
conduct](https://sutty.nl/en/code-of-conduct/).

If you like our plugins, [please consider
donating](https://donaciones.sutty.nl/en/)!

## License

The gem is available as free software under the terms of the GPL3
License.

## Code of Conduct

Everyone interacting in the jekyll-linked-posts project’s codebases,
issue trackers, chat rooms and mailing lists is expected to follow the
[code of conduct](https://sutty.nl/en/code-of-conduct/).
