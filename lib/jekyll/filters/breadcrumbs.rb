# frozen_string_literal: true

module Jekyll
  module Filters
    module Breadcrumbs
      # The breadcrumbs filter returns an Array of Jekyll::Documents
      # that are linked recursively by the same field.
      #
      # @param [Jekyll::Document]
      # @param [String]
      # @return [Array]
      def breadcrumbs(input, field)
        return unless input.respond_to? :[]

        crumbs = [ input ]
        prev   =   input

        while prev&.public_send(:[], field)
          crumbs << (prev = prev[field])
        end

        crumbs.reverse
      end
    end
  end
end

Liquid::Template.register_filter(Jekyll::Filters::Breadcrumbs)
