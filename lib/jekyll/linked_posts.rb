# frozen_string_literal: true

module Jekyll
  # Link posts by a field value, ideally an UUID
  class LinkedPosts
    # Default linked fields
    FIELDS = %w[lang related].freeze
    # Value types allowed to index by
    INDEXABLE_TYPES = [String, Integer].freeze

    attr_reader :site

    def initialize(site)
      @site = site
    end

    # Process all the documents from the site and replace the UUID for
    # the actual post.
    #
    # If it doesn't find anything it'll show a warning and replace the
    # value for nil.
    def read
      indexable_documents.each do |doc|
        process doc
      end
    end

    private

    # Processes fields from a document-like object
    #
    # @param :doc [Jekyll::Document,Jekyll::Page]
    # @return [nil]
    def process(doc)
      fields.each do |field|
        next if (values = doc.data[field]).nil?

        # XXX: this lacks... elegance
        doc.data[field] =
          case values
          when String then find(values)
          when Array
            values.map do |doc|
              find doc
            end.compact
          when Hash
            values.map do |f, value|
              [f, v] if (v = find value)
            end.compact.to_h
          else
            Jekyll.logger.warn "Couldn't link posts on #{doc.relative_path}"
            nil
          end
      end

      nil
    end

    def indexable_documents
      @indexable_documents ||= site.documents + site.pages
    end

    # The field to index posts by
    #
    # @return [String] by default uuid
    def indexed_field
      @indexed_field ||= site.config.fetch('indexed_field', 'uuid')
    end

    # Obtain docs from the whole site, indexing them by field value and
    # warning if docs have missing field, duplicate values or invalid
    # types.
    #
    # @return [Hash]
    def indexed_documents
      @indexed_documents ||= indexable_documents.reduce({}) do |docs, doc|
        index_value = doc.data[indexed_field]

        unless index_value
          Jekyll.logger.warn "#{doc.relative_path} is missing field #{indexed_field}"
          next docs
        end

        unless INDEXABLE_TYPES.include? index_value.class
          Jekyll.logger.warn "#{doc.relative_path} value for #{indexed_field} is #{index_value.class.downcase} instead of string or integer"
          next docs
        end

        if (dup = docs[index_value]) && dup != doc
          Jekyll.logger.warn "#{doc.relative_path} shares the same indexing value as #{dup.relative_path}: #{index_value}"
          next docs
        end

        docs.tap do |d|
          d[index_value] = doc
        end
      end
    end

    # Search for posts in these front matter fields.  You can add more
    # as an Array called 'fields' on _config.yml for the plugin
    # configuration:
    #
    # linked_fields:
    #   - related
    #   - lang
    #
    # @return [Array]
    def fields
      @fields ||= site.config.fetch('linked_fields', FIELDS).compact.uniq
    end

    # Find a post with the given value and log alert if missing.
    #
    # @return [Jekyll::Document,Nil]
    def find(value)
      return value if value.is_a? Jekyll::Document
      return value if value.is_a? Jekyll::Page

      indexed_documents[value].tap do |i|
        Jekyll.logger.warn "#{value} couldn't be found" if i.nil?
      end
    end
  end
end
