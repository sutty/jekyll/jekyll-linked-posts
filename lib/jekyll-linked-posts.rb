require_relative 'jekyll/linked_posts'
require_relative 'jekyll/filters/breadcrumbs'

# Process the site replacing UUIDs for posts in certain front matter fields
Jekyll::Hooks.register :site, :post_read do |site|
  Jekyll::LinkedPosts.new(site).read
end
